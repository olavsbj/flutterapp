import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../services/weather.dart';
import '../services/weather_recomandation.dart';
import '../services/location_data.dart';
import '../utilities/constants.dart';

class SecondWeatherScreen extends StatefulWidget {
  @override
  _SecondWeatherScreenState createState() => _SecondWeatherScreenState();
}

class _SecondWeatherScreenState extends State<SecondWeatherScreen> {

  Weather weatherModel;
  WeatherRecomandation recomandation;
  bool loading;

  getWeatherData()async{
    weatherModel = Weather(await LocationData(context).getCurrentWeatherData());
    recomandation =  WeatherRecomandation(weatherModel.temperature);
    loading = false;
    setState(() {

    });
  }

  initState(){
    super.initState();
    loading = true;
  }

  @override
  Widget build(BuildContext context) {
    if(loading == true){
      getWeatherData();
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/foss.jpg'),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
              Colors.white.withOpacity(0.8), BlendMode.dstATop),
        ),
      ),
      child: Expanded(
        child: Center(
          child: SpinKitChasingDots(
            color: Colors.white,
          )
        ),
      )
    );
    } else {
      return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/foss.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.8), BlendMode.dstATop),
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            margin: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    FlatButton(
                      child: Icon(Icons.keyboard_return, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
                Expanded(
                  child: Text(
                    'Det er ${weatherModel.temperature.toString()}° ved ${weatherModel.city} i dag',
                    style: kWeatherScreenStyle,
                  ),
                ),
                Expanded(
                  child: Text(
                    'Det er meldt: \n' + weatherModel.description,
                    style: kWeatherScreenStyle,
                    textAlign: TextAlign.right,
                  ),
                ),
                Expanded(
                  child: Text(
                    recomandation.getRecomandation(),
                    style: kWeatherScreenStyle,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}




import 'package:flutter/material.dart';
import 'package:flutter_phone_state/flutter_phone_state.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';



class CVScreen extends StatefulWidget {
  @override
  _CVScreenState createState() => _CVScreenState();
}

class _CVScreenState extends State<CVScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('KONTAKT INFO'),
        backgroundColor: Colors.red,
      ),
      backgroundColor: Colors.red,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 50,
            ),
            Container(
                child: CircleAvatar(
                  //radius: 80,
                  backgroundImage: AssetImage("images/profilbilde.jpg"),
                ),
                width: 180,
                height: 180,
                padding: EdgeInsets.all(5),
                decoration: new BoxDecoration(
                  color: Colors.orangeAccent.shade200, // border color
                  shape: BoxShape.circle,
                )
            ),
            Text(
              "Olav Bjørlykke",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 40,
                fontFamily: 'fasterOne'
              ),
            ),
            Text(
              "BRA KAR",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.orange.shade100,
                fontSize: 30,
                fontFamily: 'zillaSlab',
                letterSpacing: 7,
                fontWeight: FontWeight.bold,
              ),
            ),
            Container(
              width: 150,
              child: Divider(
                color: Colors.white,
              ),
            ),
            Card(
              color: Colors.white,
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: GestureDetector(
                  onTap:(){
                    FlutterPhoneState.startPhoneCall('+4799208803');
                  },
                  child: ListTile(
                    leading: Container(
                      child: Icon(
                          Icons.phone,
                          color: Colors.orangeAccent
                      ),
                    ),
                    title:Text(
                      "99208803",
                      style: TextStyle(
                        color: Colors.orangeAccent,
                        fontFamily: "SourceSansPro",
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Card(
              color: Colors.white,
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: GestureDetector(
                  onTap:(){
                    FlutterEmailSender.send(
                      Email(
                        recipients: ['olavbj@gmail.com'],
                        subject: 'Kontakt Olav',
                        body: 'Hei Olav, jeg tar kontakt fra appen din'
                      ),
                    );
                  },
                  child: ListTile(
                    leading: Icon(
                        Icons.mail_outline,
                        color: Colors.orangeAccent
                    ),
                    title: Text(
                      " olavsbj@stud.ntnu.no",
                      style: TextStyle(
                        color: Colors.orangeAccent,
                        fontFamily: "SourceSansPro",
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

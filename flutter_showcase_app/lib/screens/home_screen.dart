import 'package:OlavBjorlykke/services/geolocater.dart';
import 'package:flutter/material.dart';
import '../utilities/button_navigator.dart';
import 'cv_screen.dart';
import 'ship_screen.dart';
import '../unused/weather_screen.dart';
import '../utilities/constants.dart';
import '../services/location_data.dart';
import 'second_weather_screen.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {



  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/fjell.jpg'),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
              Colors.white.withOpacity(0.8), BlendMode.dstATop),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: TyperAnimatedTextKit(
                text: ['En app av Olav Bjørlykke'],
                textStyle: kHomeScreenStyle,
                isRepeatingAnimation: false,
                speed: Duration(milliseconds: 100),
              ),
            ),
            ButtonNavigator(
              buttonText: 'Kontaktinfo til  Olav',
              textFontSize: 20,
              screen: CVScreen(),
            ),

            ButtonNavigator(
              buttonText:'Beregn motstand på skip',
              textFontSize: 20,
              screen: ShipScreen(),
            ),
            ButtonNavigator(
              buttonText:'Se været',
              textFontSize: 20,
              screen: SecondWeatherScreen(),
            )
          ],
        ),
      ),
    );
  }
}

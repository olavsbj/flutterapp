import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../utilities/constants.dart';
import '../services/ship_calculation.dart';


class ShipCalculationScreen extends StatefulWidget {
  ShipCalculationScreen({@required this.shipLength, @ required this.shipVelocity, @required this.shipBlockCoeficient});

  final double shipLength;
  final double shipVelocity;
  final double shipBlockCoeficient;

  @override
  _ShipCalculationScreenState createState() => _ShipCalculationScreenState();
}

class _ShipCalculationScreenState extends State<ShipCalculationScreen> {
  ShipCalculation calculation;

  @override
  void initState() {
    super.initState();
    calculation = ShipCalculation(length: widget.shipLength, velocity: widget.shipVelocity, cB: widget.shipBlockCoeficient);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text('SKIP')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text('Motstanden på Skipet er:', style:kShipScreenStyle,
            textAlign: TextAlign.center,
          ),
          Text(calculation.getResistance() + 'MN', style: kShipScreenStyle,
            textAlign: TextAlign.center,
          ),
          Text('Denne dataen er ikke nøyaktig, grundigere beregninger bør derfor foretas i prosjekteringsprosessen',
            style: kDisclaimerStyle,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}

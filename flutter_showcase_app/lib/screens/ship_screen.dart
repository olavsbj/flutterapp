import 'package:OlavBjorlykke/screens/ship_calculation_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../utilities/reusable_card.dart';
import '../utilities/constants.dart';
import '../utilities/round_icon_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../utilities/button_navigator.dart';

class ShipScreen extends StatefulWidget {
  @override
  _ShipScreenState createState() => _ShipScreenState();
}

class _ShipScreenState extends State<ShipScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setImagePadding();
  }

  double length = 100.0;
  double minLength = 50.0;
  double maxLength = 150.0;
  double padding;
  double velocity = 10;
  double blockCoefecient = 0.8;

  void setImagePadding() {
    padding = 200 - length;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('SKIP')),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 5,
              child: ReusableCard(
                cardChild: Container(
                  margin: EdgeInsets.all(5),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Icon(FontAwesomeIcons.ship, size: length/1.5),
                  )
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: ReusableCard(
                  cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('LENGDE', style:kShipUnitsStyle),
                  Text(
                    length.toInt().toString() + 'm',
                    style: kShipScreenStyle,
                  ),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                        thumbShape: RoundSliderThumbShape(
                            enabledThumbRadius: 15, disabledThumbRadius: 10),
                        thumbColor: Color(0xFFEB1555),
                        overlayColor: Color(0x46EB1555),
                        activeTrackColor: Colors.white),
                    child: Slider(
                      value: length,
                      min: minLength,
                      max: maxLength,
                      inactiveColor: Color(0xFF8D8E98),
                      onChanged: (double newValue) {
                        setState(() {
                          length = newValue.toDouble();
                          setImagePadding();
                        });
                      },
                    ),
                  ),
                ],
              )),
            ),
            Expanded(
              flex: 5,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: ReusableCard(
                      cardChild: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('HASTIGHET m/s', style: kShipUnitsStyle),
                          Text(velocity.toString(), style: kShipScreenStyle),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              RoundIconButton(
                                icon: FontAwesomeIcons.minus,
                                onPush: () {
                                  setState(() {
                                    if(velocity > 0)
                                    {velocity--;}
                                  });
                                },
                              ),
                              SizedBox.fromSize(
                                size: Size(10, 10),
                              ),
                              RoundIconButton(
                                  icon: FontAwesomeIcons.plus,
                                  onPush: () {
                                    setState(() {
                                      if(velocity < 50)
                                      {velocity++;}
                                    });
                                  }),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                      child: ReusableCard(
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('BLOKK \n KOEFFISIENT', style: kShipUnitsStyle,
                          textAlign: TextAlign.center,
                        ),
                        Text(blockCoefecient.toStringAsPrecision(2),
                            style: kShipScreenStyle),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RoundIconButton(
                              icon: FontAwesomeIcons.minus,
                              onPush: () {
                                setState(() {
                                  if (blockCoefecient > 0.31) {
                                    blockCoefecient -= 0.01;
                                  }
                                });
                              },
                            ),
                            SizedBox.fromSize(
                              size: Size(10, 10),
                            ),
                            RoundIconButton(
                                icon: FontAwesomeIcons.plus,
                                onPush: () {
                                  setState(() {
                                    if (blockCoefecient < 1.0) {
                                      blockCoefecient += 0.01;
                                    }
                                  });
                                }),
                          ],
                        ),
                      ],
                    ),
                  ))
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: ButtonNavigator(
                textFontSize: 14,
                color: Colors.red.shade700,
                buttonText: 'KALKULER',
                screen: ShipCalculationScreen(shipLength: length, shipVelocity: velocity, shipBlockCoeficient: blockCoefecient,),
              ),
            ),
          ],
        ));
  }
}

import 'package:flutter/material.dart';
import 'screens/home_screen.dart';
import 'screens/home_screen.dart';
import 'screens/ship_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        primaryColor: Color(0xFF0A0D22),
        scaffoldBackgroundColor: Color(0xFF0A0D22),
        textTheme: TextTheme(
          body1: TextStyle(color: Colors.white),

        ),
      ),
      home: HomeScreen(),
    );
  }
}

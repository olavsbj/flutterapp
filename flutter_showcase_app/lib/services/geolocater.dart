import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import '../utilities/constants.dart';


class Geolocater{

  double longtitude;
  double latitude;

  Future getCurrentLocation()async{
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.low);
    longtitude = position.longitude;
    latitude = position.latitude;
    print('succesfull geolocation');
  }
}


import 'package:http/http.dart'as http;
import 'dart:convert';

class NetworkDatacall{
  NetworkDatacall(this.url);

  String url;

  Future getData() async {
    http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      print('api call succesful');
      return jsonDecode(response.body);
    } else {
      print(response.statusCode);
    }
  }



}
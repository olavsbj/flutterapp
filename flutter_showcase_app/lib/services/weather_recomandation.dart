

class WeatherRecomandation{

  WeatherRecomandation(this.temperature);

  final int temperature;

  String getRecomandation(){
    if (temperature > 25){
      return 'I dag er det varmt, shorts og T-skjorte blir bra';
    }
    else if (temperature > 15){
      return 'Det holder kanskje med bukse og T-skjorte, men ta med jakke for sikkerhets skyld';
    }
    else if (temperature > 0){
      return 'Det er ganske kjølig idag, så husk å ta på nok klær';
    }
    else{
      return 'Det er iskaldt ute, finn frem votter og skjerf';
    }
  }
}
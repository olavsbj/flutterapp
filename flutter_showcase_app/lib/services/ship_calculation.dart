import 'package:flutter/material.dart';
import 'dart:math';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class ShipCalculation{

  ShipCalculation({@required this.length, @required this.velocity, @required this.cB}){
    beam = this.length/10;
    draft = this.beam/2;
  }

  double length;
  double cB;
  double velocity;
  double resistance;
  double rho = 1000;
  double beam;
  double draft;

  double cT = 6*pow(10,-3);

  double calculateResistance(){
    resistance = cT * 0.5 * rho * pow(velocity, 2) * (length * beam* draft)*cB;
    return resistance;
  }
  String getResistance(){
    var shipResistance = calculateResistance();
    shipResistance = shipResistance/pow(10,6);
    var resistanceAsString = shipResistance.toStringAsPrecision(3);
    return resistanceAsString;
  }
}
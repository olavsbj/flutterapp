import 'package:OlavBjorlykke/services/geolocater.dart';
import 'package:flutter/material.dart';
import '../unused/weather_screen.dart';
import '../services/network_datacall.dart';
import '../utilities/constants.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LocationData {
  LocationData(this.context);

  var context;
  Geolocater location;
  dynamic weatherData;

  Future getCurrentWeatherData() async {
    if (location == null) {
      Geolocater location = Geolocater();
      await location.getCurrentLocation();

      NetworkDatacall datacall = NetworkDatacall(
          'https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longtitude}&appid=$OpenWeatherMapApiKey&units=metric&lang=no');

      weatherData = await datacall.getData();

      return weatherData;
    }
  }

  Future getCurrentLocationData() async {
    if (location == null) {
      Geolocater location = Geolocater();
      await location.getCurrentLocation();

      NetworkDatacall datacall = NetworkDatacall(
          'https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longtitude}&appid=$OpenWeatherMapApiKey&units=metric&lang=no');

      weatherData = await datacall.getData();

      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return WeatherScreen(weatherData);
      }));
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return WeatherScreen(weatherData);
      }));
    }
  }
}

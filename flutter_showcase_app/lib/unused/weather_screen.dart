import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../services/geolocater.dart';
import '../services/weather.dart';
import '../utilities/constants.dart';
import '../screens/home_screen.dart';
import '../services/weather_recomandation.dart';

class WeatherScreen extends StatefulWidget {
  WeatherScreen(this.weatherData);

  final dynamic weatherData;

  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  Weather weatherModel;
  WeatherRecomandation recomandation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    weatherModel = Weather(widget.weatherData);
    recomandation = WeatherRecomandation(weatherModel.temperature);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/foss.jpg'),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
              Colors.white.withOpacity(0.8), BlendMode.dstATop),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          margin: EdgeInsets.all(5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    child: Icon(Icons.keyboard_return, color: Colors.white),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
              Expanded(
                child: Text(
                  'Det er ${weatherModel.temperature.toString()}° ved ${weatherModel.city} i dag',
                  style: kWeatherScreenStyle,
                ),
              ),
              Expanded(
                child: Text(
                  'Det er meldt: \n' + weatherModel.description,
                  style: kWeatherScreenStyle,
                  textAlign: TextAlign.right,
                ),
              ),
              Expanded(
                child: Text(
                  recomandation.getRecomandation(),
                  style: kWeatherScreenStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

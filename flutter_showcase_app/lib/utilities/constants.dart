import 'package:flutter/material.dart';

const String OpenWeatherMapApiKey = '819a254ab671ec6b36b50390d54489b0';

const String baseUrl = 'https://api.openweathermap.org/data/2.5/weather?';

const Color activeCardColor = Color(0xFF1D1E33);


const kWeatherScreenStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 40,
  fontFamily: 'ZillaSlab'
);

const kShipScreenStyle = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontSize: 40,
    fontFamily: 'SourceSansPro'
);

const kShipUnitsStyle = TextStyle(
    color: Colors.blueGrey,
    fontWeight: FontWeight.bold,
    fontSize: 20,
    fontFamily: 'SourceSansPro'
);

const kHomeScreenStyle= TextStyle(
    color: Colors.blueGrey,
    fontWeight: FontWeight.bold,
    fontSize: 20,
    fontFamily: 'ZillaSlab'
);

const kDisclaimerStyle= TextStyle(
    color: Colors.red,
    fontSize: 15,
    fontFamily: 'ZillaSlab'
);



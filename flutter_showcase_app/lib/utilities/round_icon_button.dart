import 'package:flutter/material.dart';


class RoundIconButton extends StatelessWidget {
  @override
  RoundIconButton({this.icon, this.onPush});

  final IconData icon;
  final Function onPush;

  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: BoxConstraints.tightFor(width: 56, height: 56),
      onPressed: onPush,
      child: Icon(icon),
      shape: CircleBorder(),
      fillColor: Color(0xFF4C4F5E),
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonNavigator extends StatelessWidget {

  ButtonNavigator({this.buttonText, this.screen, this.color = Colors.transparent, this.textFontSize});

  final String buttonText;
  final screen;
  final Color color;
  final double textFontSize;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      child: RaisedButton(
        color: color,
        padding: EdgeInsets.all(10),
        child: Text(
            buttonText,
            style: TextStyle(color: Colors.white, fontSize: textFontSize)
        ),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder:(context){
            return screen;
          }));
        },
      ),
    );
  }
}
